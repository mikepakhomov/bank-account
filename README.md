# Bank Account application

## Technologies

Scala, Play, Anorm for database access, H2 database, scoverage for code coverage. Java 8 is required to run the code. 

## How to run

- clone the project
- `cd bank-account`
- run tests with coverage `sbt clean coverage test`
- show coverage report `sbt coverageReport`
- run the application `sbt run`
- deposit 20000 `curl -i --data "amount=20000" http://localhost:9000/deposit`
- withdraw 10000 `curl -i --data "amount=10000" http://localhost:9000/withdrawal`
- show balance `curl -i -X GET http://localhost:9000/balance` which should yeild `10000`

## Implementation notes

- I assumed the web service is for one account only and is open to the world. 
  The only user in the system is `user_id = 1`
- There are many ways to handle concurrent transactions:
    - In my former projects we used Oracle DB and various kinds of pessimistic locks. 
      In this mini project I applied the same techniques of row-level locking (`SELECT FOR UPDATE`)
    - However, in addition to that I also implemented concurrent modifications coordination in scala
      code (just to be sure and to demonstrate my knowledge of java/scala concurrency). I wrote a wrapper
      over java `ReentrantReadWriteLock` using the `loan` pattern.
      
## Database schema
      
```sql
create table balance (
  user_id                        bigint not null,
  amount                         decimal not null,
  constraint pk_user primary key (user_id))
;

create table transactions (
  id                        bigint not null,
  user_id                   bigint not null,
  type                      varchar(8),
  amount                    decimal,
  tmstamp                   timestamp,
  constraint pk_transaction primary key (id))
;
```