package concurrency

/**
  * This object provides methods to coordinate concurrent transactions.
  * Before doing a deposit or withdraw operation, a thread must acquire a write lock
  */
object Coordination {

  /**
    * final and immutable. it's safe to read from the map without syncronization
    */
  val locks = Map(1 -> ReadWriteLock())

  def lockForUser(userId: Int): ReadWriteLock = locks(userId)

}
