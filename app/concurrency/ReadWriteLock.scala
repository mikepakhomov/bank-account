package concurrency

import java.util.concurrent.locks.ReentrantReadWriteLock

/**
  * A wrapper over java `ReentrantReadWriteLock`. I'm using a `loan` pattern to automatically release lock.
  * The trick here is a block passed as a `by name` parameter
  */
class ReadWriteLock {

  private[this] val rwLock = new ReentrantReadWriteLock();

  def withReadLock[B](fn: =>B): B = {
    rwLock.readLock().lock()
    try { fn }
    finally { rwLock.readLock().unlock() }
  }

  def withWriteLock[B](fn: =>B) : B = {
    rwLock.writeLock().lock()
    try { fn }
    finally { rwLock.writeLock().unlock() }
  }

}

object ReadWriteLock {
  def apply(): ReadWriteLock = new ReadWriteLock()
}
