package controllers

import javax.inject.{Inject, Singleton}

import model.TransactionError
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import services.BankAccountService

import scala.util.{Failure, Success, Try}

/**
  * Slim controller. All business logic is inside the service
  */
@Singleton
class BankAccountController @Inject() (service: BankAccountService)
  extends Controller {

  def balance = Action { implicit request =>
    service.balance match {
      case Success(amount) => Ok(Json.toJson(amount))
      case Failure(e) =>
        Logger.error(e.getMessage)
        InternalServerError(Json.toJson("Internal Server Error"))
    }
  }

  def deposit = Action { request =>
    Try(BigDecimal(request.body.asFormUrlEncoded.get("amount")(0).toDouble)) match {
      case Success(amount) => service.deposit (amount) match {
        case Right (msg) => Ok (msg)
        case Left (TransactionError (BAD_REQUEST, msg) ) => BadRequest (msg)
        case Left (TransactionError (_, msg) ) => InternalServerError (msg)
      }
      case Failure(e) => BadRequest(e.toString)
    }
  }

  def withdrawal = Action { request =>
    Try(BigDecimal(request.body.asFormUrlEncoded.get("amount")(0).toDouble)) match {
      case Success(amount) => service.withdrawal (amount) match {
        case Right (msg) => Ok (msg)
        case Left (TransactionError (BAD_REQUEST, msg) ) => BadRequest (msg)
        case Left (TransactionError (_, msg) ) => InternalServerError (msg)
      }
      case Failure(e) => BadRequest(e.toString)
    }
  }
}
