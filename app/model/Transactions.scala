package model

sealed trait TransactionType
case object Deposit extends TransactionType
case object WithDraw extends TransactionType

case class TransactionError(status: Int, message: String)