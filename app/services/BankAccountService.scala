package services

import java.sql.{Connection, Date}
import java.time.ZoneId
import javax.inject.{Inject, Singleton}

import anorm.SqlParser._
import anorm._
import concurrency.Coordination
import model.{Deposit, TransactionError, TransactionType, WithDraw}
import play.api.Configuration
import play.api.db.DBApi

import scala.util.Try
import scala.util.control.NonFatal
import play.api.http.Status._
import play.api.Logger

@Singleton
class BankAccountService @Inject() (dbapi: DBApi, config: Configuration) {

  private val db = dbapi.database("default")
  val userId = config.getInt("app.user_id").getOrElse(1)

  /**
    * service method for `balance` REST endpoint
    */
  def balance: Try[BigDecimal] = Try {
    // here I can use a readLock, but even without the lock it's fine.
    // let's rely on the database to handle concurrency issues
    db.withTransaction { implicit connection =>
      getBalance
    }
  }

  /**
    * service method for `deposit` endpoint
    */
  def deposit(amount: BigDecimal): Either[TransactionError, String] = {
    try {
      Coordination.lockForUser(userId).withWriteLock {
        db.withTransaction { implicit connection =>
          // row-level locking
          selectForUpdate

          if (!checkAmountPositive(amount)) {
            Left(TransactionError(BAD_REQUEST, "Amount Should Be Positive"))
          } else if (!checkMaxDepositForTheDay(amount)) {
            Left(TransactionError(BAD_REQUEST, "Exceeded Maximum Deposit for the Day ($150K)"))
          } else if (!checkMaxDepositPerTransaction(amount)) {
            Left(TransactionError(BAD_REQUEST, "Exceeded Maximum Deposit Per Transaction ($40K))"))
          } else if (!checkMaxDepositFrequency(amount)) {
            Left(TransactionError(BAD_REQUEST, "Exceeded Maximum Deposit Frequency (4 transactions/day)"))
          } else {
            insertTransaction(amount, Deposit)
            updateBalance(amount, Deposit)
            Right("Transaction succeeded")
          }
        }
      }
    } catch {
      case NonFatal(e) =>
        Logger.error(e.getMessage)
        Left(TransactionError(INTERNAL_SERVER_ERROR, "Internal Server Error"))
    }
  }

  /**
    * service method for `withdrawal` endpoint
    */
  def withdrawal(amount: BigDecimal): Either[TransactionError, String] = {
    try {
      Coordination.lockForUser(userId).withWriteLock {
        db.withTransaction { implicit connection =>
          // row-level locking
          selectForUpdate

          if (!checkAmountPositive(amount)) {
            Left(TransactionError(BAD_REQUEST, "Amount Should Be Positive"))
          } else if (!checkMaxWithdrawalForTheDay(amount)) {
            Left(TransactionError(BAD_REQUEST, "Exceeded Maximum Withdrawal for the Day ($50K)"))
          } else if (!checkMaxWithdrawalPerTransaction(amount)) {
            Left(TransactionError(BAD_REQUEST, "Exceeded Maximum Withdrawal Per Transaction ($20K)"))
          } else if (!checkMaxWithdrawalFrequency(amount)) {
            Left(TransactionError(BAD_REQUEST, "Exceeded Maximum Withdrawal Frequency (3 transactions/day)"))
          } else if (!checkBalance(amount)) {
            Left(TransactionError(BAD_REQUEST, "Not enough money"))
          } else {
            insertTransaction(amount, WithDraw)
            updateBalance(amount, WithDraw)
            Right("Transaction succeeded")
          }
        }
      }
    } catch {
      case NonFatal(e) =>
        Logger.error(e.getMessage)
        Left(TransactionError(INTERNAL_SERVER_ERROR, "Internal Server Error"))
    }
  }

  def checkAmountPositive(amount: BigDecimal): Boolean = amount >= 0

  def checkMaxDepositPerTransaction(amount: BigDecimal): Boolean = amount <= 40000

  def checkMaxDepositForTheDay(amount: BigDecimal)
                              (implicit connection: Connection): Boolean = {
    val depositForTheDay = SQL(
      s"""
        select sum(amount) from TRANSACTIONS
        where trunc(TMSTAMP) = {today} and type = '${Deposit.toString}' and user_id = {user_id}
      """
    ).on(
      'today -> today,
      'user_id -> userId
    ).as(scalar[BigDecimal].singleOpt).getOrElse(BigDecimal(0))
    depositForTheDay + amount <= 150000
  }

  def checkMaxDepositFrequency(amount: BigDecimal)
                              (implicit connection: Connection): Boolean = {
    val transactionsForTheDay = SQL(
      s"""
         select count(1) from TRANSACTIONS
         where trunc(TMSTAMP) = {today} and type = '${Deposit.toString}' and user_id = {user_id}
      """
    ).on(
      'today -> today,
      'user_id -> userId
    ).as(scalar[BigDecimal].singleOpt).getOrElse(BigDecimal(0))
    transactionsForTheDay + 1 <= 4
  }

  def today: java.util.Date =
    java.util.Date.from(java.time.LocalDate.now.atStartOfDay(ZoneId.systemDefault()).toInstant())

  def selectForUpdate(implicit connection: Connection): Unit = {
    SQL("select amount from balance where user_id = {user_id} for update"
    ).on(
      'user_id -> userId
    ).execute()
  }

  def insertTransaction(amount: BigDecimal, transactionType: TransactionType)(implicit connection: Connection): Unit = {
    SQL(
      """
        |insert into transactions(id, user_id, type, amount, tmstamp)
        |values ((select next value for transaction_seq), {user_id}, {transaction_type}, {amount}, current_timestamp())
      """.stripMargin
    ).on(
      'user_id -> userId,
      'transaction_type -> transactionType.toString,
      'amount -> amount
    ).executeUpdate()
  }

  def updateBalance(amount: BigDecimal, transactionType: TransactionType)(implicit connection: Connection): Unit = {
    val updateAmount = transactionType match {
      case Deposit => amount
      case WithDraw => -amount
    }
    SQL(
      """
        |update balance set amount = amount + {amount}
        |where user_id = {user_id}
      """.stripMargin
    ).on(
      'user_id -> userId,
      'amount -> updateAmount
    ).executeUpdate()
  }

  def checkMaxWithdrawalForTheDay(amount: BigDecimal)
                              (implicit connection: Connection): Boolean = {
    val withdrawalForTheDay = SQL(
      s"""
        select sum(amount) from TRANSACTIONS
        where trunc(TMSTAMP) = {today} and type = '${WithDraw.toString}' and user_id = {user_id}
      """
    ).on(
      'today -> today,
      'user_id -> userId
    ).as(scalar[BigDecimal].singleOpt).getOrElse(BigDecimal(0))
    withdrawalForTheDay + amount <= 50000
  }

  def checkMaxWithdrawalPerTransaction(amount: BigDecimal): Boolean = amount <= 20000

  def checkMaxWithdrawalFrequency(amount: BigDecimal)
                              (implicit connection: Connection): Boolean = {
    val transactionsForTheDay = SQL(
      s"""
         select count(1) from TRANSACTIONS
         where trunc(TMSTAMP) = {today} and type = '${WithDraw.toString}' and user_id = {user_id}
      """
    ).on(
      'today -> today,
      'user_id -> userId
    ).as(scalar[BigDecimal].singleOpt).getOrElse(BigDecimal(0))
    transactionsForTheDay + 1 <= 3
  }

  def getBalance(implicit connection: Connection): BigDecimal = {
    SQL(
      """
          select amount from balance
          where user_id = {user_id}
        """
    ).on(
      'user_id -> userId
    ).as(scalar[BigDecimal].single)
  }

  def checkBalance(amount: BigDecimal)
                  (implicit connection: Connection): Boolean = {
    val curBalance = getBalance
    curBalance - amount >= 0
  }

}
