name := """bank-account"""
organization := "com.mpakhomov"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  jdbc,
  evolutions,
  "com.typesafe.play" %% "anorm" % "2.5.2",
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test
)

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.mpakhomov.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.mpakhomov.binders._"
