# --- First database schema

# --- !Ups

set ignorecase true;

create table balance (
  user_id                        bigint not null,
  amount                         decimal not null,
  constraint pk_user primary key (user_id))
;

create table transactions (
  id                        bigint not null,
  user_id                   bigint not null,
  type                      varchar(8),
  amount                    decimal,
  tmstamp                   timestamp,
  constraint pk_transaction primary key (id))
;

insert into balance (user_id, amount) values (1, 0.0);

create sequence transaction_seq start with 1;

alter table transactions add constraint fk_user foreign key (user_id) references balance (user_id) on delete restrict on update restrict;

# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists balance;

drop table if exists transactions;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists transaction_seq;