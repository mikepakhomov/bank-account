package controllers

package controllers

import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play._
import play.api.test.Helpers._
import play.api.test._

class BankAccountControllerBalanceSpec extends PlaySpec with OneAppPerSuite with ScalaFutures {

  def bankAccountController = app.injector.instanceOf(classOf[BankAccountController])

  "BankAccountController" should {
    //    running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

    "show balance 0 at the beginning" in {
      val result = bankAccountController.balance(FakeRequest())
      status(result) must equal(OK)
      contentAsString(result) must include("0")
    }

    "deposit 10000" in {
      val result = bankAccountController.deposit(
        FakeRequest().withFormUrlEncodedBody("amount" -> "10000")
      )
      status(result) must equal(OK)
      contentAsString(result) must include("Transaction succeeded")
    }

    "show balance 10000" in {
      val result = bankAccountController.balance(FakeRequest())
      status(result) must equal(OK)
      contentAsString(result) must include("10000")
    }


    "reject withdraw 15000, because it's more than current balance 10000" in {
      val result = bankAccountController.withdrawal(
        FakeRequest().withFormUrlEncodedBody("amount" -> "15000")
      )
      status(result) must equal(BAD_REQUEST)
      contentAsString(result) must include("Not enough money")
    }

    "have balance unchanged (10000)" in {
      val result = bankAccountController.balance(FakeRequest())
      status(result) must equal(OK)
      contentAsString(result) must include("10000")
    }

    //  }
  }

}

