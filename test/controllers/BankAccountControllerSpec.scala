package controllers

import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play._
import play.api.test.Helpers._
import play.api.test._

class BankAccountControllerSpec extends PlaySpec with OneAppPerSuite with ScalaFutures {

  def bankAccountController = app.injector.instanceOf(classOf[BankAccountController])

  "BankAccountController" should {
    //    running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

    "show balance 0 at the beginning" in {
      val result = bankAccountController.balance(FakeRequest())
      status(result) must equal(OK)
      contentAsString(result) must include("0")
    }

    "reject negative amounts in deposit" in {
      val result = bankAccountController.deposit(
        FakeRequest().withFormUrlEncodedBody("amount" -> "-1")
      )
      status(result) must equal(BAD_REQUEST)
      contentAsString(result) must include("Amount Should Be Positive")
    }

    "deposit 40000" in {
      val result = bankAccountController.deposit(
        FakeRequest().withFormUrlEncodedBody("amount" -> "40000")
      )
      status(result) must equal(OK)
      contentAsString(result) must include("Transaction succeeded")
    }

    "show balance 40000" in {
      val result = bankAccountController.balance(FakeRequest())
      status(result) must equal(OK)
      contentAsString(result) must include("40000")
    }

    "reject deposit 40001" in {
      val result = bankAccountController.deposit(
        FakeRequest().withFormUrlEncodedBody("amount" -> "40001")
      )
      status(result) must equal(BAD_REQUEST)
      contentAsString(result) must include("Exceeded Maximum Deposit Per Transaction ($40K))")
    }

    "deposit another 40000" in {
      val result = bankAccountController.deposit(
        FakeRequest().withFormUrlEncodedBody("amount" -> "40000")
      )
      status(result) must equal(OK)
      contentAsString(result) must include("Transaction succeeded")
    }

    "show balance 80000" in {
      val result = bankAccountController.balance(FakeRequest())
      status(result) must equal(OK)
      contentAsString(result) must include("80000")
    }

    "reject malformed input in deposit" in {
      val result = bankAccountController.deposit(
        FakeRequest().withFormUrlEncodedBody("amount" -> "FOOBAR")
      )
      status(result) must equal(BAD_REQUEST)
    }


    "deposit another 40000, with total 120000" in {
      val result = bankAccountController.deposit(
        FakeRequest().withFormUrlEncodedBody("amount" -> "40000")
      )
      status(result) must equal(OK)
      contentAsString(result) must include("Transaction succeeded")
    }

    "reject deposit 40000, because it exceeds limit 150000" in {
      val result = bankAccountController.deposit(
        FakeRequest().withFormUrlEncodedBody("amount" -> "40000")
      )
      status(result) must equal(BAD_REQUEST)
      contentAsString(result) must include("Exceeded Maximum Deposit for the Day ($150K)")
    }


    "deposit 20000" in {
      val result = bankAccountController.deposit(
        FakeRequest().withFormUrlEncodedBody("amount" -> "20000")
      )
      status(result) must equal(OK)
      contentAsString(result) must include("Transaction succeeded")
    }

    "show balance 140000" in {
      val result = bankAccountController.balance(FakeRequest())
      status(result) must equal(OK)
      contentAsString(result) must include("140000")
    }

    "reject deposit 1000, because it exceeds maximum deposit frequency per day" in {
      val result = bankAccountController.deposit(
        FakeRequest().withFormUrlEncodedBody("amount" -> "1000")
      )
      status(result) must equal(BAD_REQUEST)
      contentAsString(result) must include("Exceeded Maximum Deposit Frequency (4 transactions/day)")
    }

    "withdraw 20000" in {
      val result = bankAccountController.withdrawal(
        FakeRequest().withFormUrlEncodedBody("amount" -> "20000")
      )
      status(result) must equal(OK)
      contentAsString(result) must include("Transaction succeeded")
    }

    "show balance 120000" in {
      val result = bankAccountController.balance(FakeRequest())
      status(result) must equal(OK)
      contentAsString(result) must include("120000")
    }

    "reject malformed input in withdraw" in {
      val result = bankAccountController.withdrawal(
        FakeRequest().withFormUrlEncodedBody("amount" -> "FOOBAR")
      )
      status(result) must equal(BAD_REQUEST)
    }

    "reject withdraw 20001" in {
      val result = bankAccountController.withdrawal(
        FakeRequest().withFormUrlEncodedBody("amount" -> "20001")
      )
      status(result) must equal(BAD_REQUEST)
      contentAsString(result) must include("Exceeded Maximum Withdrawal Per Transaction ($20K)")
    }

    "withdraw  10000" in {
      val result = bankAccountController.withdrawal(
        FakeRequest().withFormUrlEncodedBody("amount" -> "10000")
      )
      status(result) must equal(OK)
      contentAsString(result) must include("Transaction succeeded")
    }

    "withdraw another 10000" in {
      val result = bankAccountController.withdrawal(
        FakeRequest().withFormUrlEncodedBody("amount" -> "10000")
      )
      status(result) must equal(OK)
      contentAsString(result) must include("Transaction succeeded")
    }

    "show balance 100000" in {
      val result = bankAccountController.balance(FakeRequest())
      status(result) must equal(OK)
      contentAsString(result) must include("100000")
    }

    "reject withdraw 5000, because it exceeds maximum withdraw frequency per day" in {
      val result = bankAccountController.withdrawal(
        FakeRequest().withFormUrlEncodedBody("amount" -> "5000")
      )
      status(result) must equal(BAD_REQUEST)
      contentAsString(result) must include("Exceeded Maximum Withdrawal Frequency (3 transactions/day)")
    }

    "have balance unchanged (100000)" in {
      val result = bankAccountController.balance(FakeRequest())
      status(result) must equal(OK)
      contentAsString(result) must include("100000")
    }

    //  }
  }

}
